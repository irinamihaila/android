package com.example.faza1;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Student.class, Mark.class}, version = 1)
public abstract class Database extends RoomDatabase {
    public abstract StudentDao studentDao();
}

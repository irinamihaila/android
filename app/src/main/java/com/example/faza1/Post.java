package com.example.faza1;

public class Post {
    private String Description, Image, Title;

    public Post(String description, String image, String title) {
        Description = description;
        Image = image;
        Title = title;
    }

    public Post(){

    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}

package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateProfileActivity extends AppCompatActivity {

    EditText etName, etEmail, etOldPassword, etNewPassword, etConfirmPassword;
    TextView tvUsernameUpdate;
    Button btnUpdateProfile;
    LoginDatabaseAdapter login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        User user;
        login = new LoginDatabaseAdapter(this);
        login = login.open();

        etName = (EditText) findViewById(R.id.etNameUpdate);
        etEmail = (EditText) findViewById(R.id.etEmailUpdate);
        etOldPassword = (EditText) findViewById(R.id.etOldPassUpdate);
        etNewPassword = (EditText) findViewById(R.id.etNewPassUpdate);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassUpdate);
        tvUsernameUpdate = (TextView)findViewById(R.id.tvUsernameUpdate);
        btnUpdateProfile=(Button)findViewById(R.id.btnUpdateProfile);
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            String value = extra.getString("user");
            user = login.getUser(value);
            etName.setText(user.getName());
            etEmail.setText(user.getEmail());
            tvUsernameUpdate.setText(user.getUsername());
        }

        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                User user2 = login.getUser(tvUsernameUpdate.getText().toString());
                if(!etOldPassword.getText().toString().equals(user2.getPassword()) || etOldPassword.equals(""))
                {
                    Toast.makeText(UpdateProfileActivity.this, "Wrong password!", Toast.LENGTH_SHORT).show();
                    etOldPassword.setText("");
                }
                else {
                    if(etNewPassword.getText().toString().equals("")&& etConfirmPassword.getText().toString().equals("")) {
                        login.updateEntry(etName.getText().toString(), etEmail.getText().toString(), tvUsernameUpdate.getText().toString(),
                                etOldPassword.getText().toString());
                        Toast.makeText(UpdateProfileActivity.this, "Updated successfully", Toast.LENGTH_SHORT).show();
                        Intent intentProfile = new Intent(getApplicationContext(), Profile.class);
                        intentProfile.putExtra("userName", tvUsernameUpdate.getText());
                        startActivity(intentProfile);
                    }
                    else
                    if(etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                        login.updateEntry(etName.getText().toString(), etEmail.getText().toString(), tvUsernameUpdate.getText().toString(),
                                etNewPassword.getText().toString());
                        Toast.makeText(UpdateProfileActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                        Intent intentProfile=new Intent(getApplicationContext(), Profile.class);
                        intentProfile.putExtra("userName", tvUsernameUpdate.getText());
                        startActivity(intentProfile);
                    }
                    else {
                        Toast.makeText(UpdateProfileActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
                        etNewPassword.setText("");
                        etConfirmPassword.setText("");
                    }
                }
            }
        });
    }
}
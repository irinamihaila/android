package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.GenericArrayType;

public class AdminLoginActivity extends AppCompatActivity {

    Button login, save;
    EditText user, pass, etName;
    public static final String Name = "nameKey";
    SharedPreferences sharedPreferences;

    public void save(View v){
        sharedPreferences = getSharedPreferences("Save", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Value", etName.getText().toString());
        editor.apply();
        Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        login = (Button)findViewById(R.id.btnLoginAdmin);
        save = (Button)findViewById(R.id.btnSaveName);
        user = (EditText)findViewById(R.id.editText);
        pass = (EditText)findViewById(R.id.editText2);
        etName = (EditText) findViewById(R.id.etNumeProfesor);

        login.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if(user.getText().toString().equals("admin") && pass.getText().toString().equals("pass")) {
                    Intent intent = new Intent(getApplicationContext(), AddGradeActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Please introduce user: admin and password: pass", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}

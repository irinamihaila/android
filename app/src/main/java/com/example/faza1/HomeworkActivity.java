package com.example.faza1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class HomeworkActivity extends AppCompatActivity {

    RecyclerView homework;
    private DatabaseReference query;
    private FirebaseRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);

        homework = (RecyclerView) findViewById(R.id.hw_list);
        homework.setHasFixedSize(true);
        homework.setLayoutManager(new LinearLayoutManager(this));
        fetch();
    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void fetch(){
        query = FirebaseDatabase.getInstance().getReference().child("Homework");
        FirebaseRecyclerOptions<Post> options =
                new FirebaseRecyclerOptions.Builder<Post>()
                        .setQuery(query, new SnapshotParser<Post>() {
                            @NonNull
                            @Override
                            public Post parseSnapshot(@NonNull DataSnapshot snapshot) {
                                return new Post(snapshot.child("Description").getValue().toString(),
                                        snapshot.child("Image").getValue().toString(),
                                        snapshot.child("Title").getValue().toString());
                            }
                        })
                        .build();

        adapter = new FirebaseRecyclerAdapter<Post, HomeworkViewHolder>(options) {
            @Override
            public HomeworkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.hw_row, parent, false);

                return new HomeworkViewHolder(view);
            }


            @Override
            protected void onBindViewHolder(HomeworkViewHolder holder, final int position, Post model) {
                holder.setTxtDesc(model.getDescription());
                holder.setImage(getApplicationContext(), model.getImage());
                holder.setTxtTitle(model.getTitle());
            }
        };
        homework.setAdapter(adapter);
    }

    public static class HomeworkViewHolder extends RecyclerView.ViewHolder{

        View view;

        public HomeworkViewHolder(View itemView) {
            super(itemView);
        }

        public void setTxtTitle(String string) {
            TextView txtTitle = (TextView)itemView.findViewById(R.id.tvPostT);
            txtTitle.setText(string);
        }


        public void setTxtDesc(String string) {
            TextView txtDesc = (TextView)itemView.findViewById(R.id.tvPostD);
            txtDesc.setText(string);
        }

        public void setImage(Context context, String string){
            ImageView img = (ImageView)itemView.findViewById(R.id.ivPost);
            Picasso.with(context).load(string).into(img);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_location){
            startActivity(new Intent(HomeworkActivity.this, PostHomeworkActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MarkReportActivity extends AppCompatActivity {

    final static String fileName = "data.txt";
    List<Mark> goodMarks = new ArrayList<>();
    EditText raport;
    Button saveTxt;
    String info = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_report);

        raport = (EditText)findViewById(R.id.etRaport);
        saveTxt = (Button)findViewById(R.id.btnSaveTxt);
        raport.setEnabled(false);

        goodMarks = MainActivity.database.studentDao().getGoodMarks();
        int i = 1;
        for(Mark m : goodMarks){
            info += i + ". " +  m.toString();
            info += "\n\n";
            i++;
        }
        raport.setText(info);

        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = raport.getText().toString();
                FileOutputStream fos = null;

                try {
                    fos = openFileOutput(fileName, MODE_PRIVATE);
                    fos.write(text.getBytes());
                    Toast.makeText(getApplicationContext(), "Saved to: " + getFilesDir() + "/" + fileName, Toast.LENGTH_LONG).show();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally{
                    if (fos!=null){
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        });
    }
}

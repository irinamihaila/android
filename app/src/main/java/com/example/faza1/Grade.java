package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Grade extends AppCompatActivity {
    Button display;
    ListView list;
    List<StudentsWithMarks> note;
    CustomAdapter c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);

        display = (Button)findViewById(R.id.btnDisplay);
        list = (ListView)findViewById(R.id.lvGrades);
        display.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                note = MainActivity.database.studentDao().getStudentsWithMarks();
                c = new CustomAdapter(getApplicationContext(), note);
                list.setAdapter(c);
            }
        });

    }

}


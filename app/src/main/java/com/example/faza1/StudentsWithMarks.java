package com.example.faza1;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Relation;

import java.util.List;

public class StudentsWithMarks {
    @Embedded public Student student;
    @Relation(
            parentColumn = "idStudent",
            entityColumn = "id",
            entity = Mark.class
    )
    public List<Mark> marks;

    @Override
    public String toString() {
        return "StudentsWithMarks{" +
                "student=" + student.toString() +
                ", marks=" + marks.toString() +
                '}';
    }
}

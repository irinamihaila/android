package com.example.faza1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class LoginDatabaseAdapter {
    public SQLiteDatabase db;
    private final Context context;
    private LoginHelp dbHelper;
    static final String DATABASE_NAME = "logins.db";
    static final int DATABASE_VERSION = 1;
    public static final int NAME_COLUMN = 1;
    static final String DATABASE_CREATE = "create table "+"LOGIN"+
            "( " +"ID"+" integer primary key autoincrement,"+ "NAME text, EMAIL text, USERNAME  text,PASSWORD text); ";


    public  LoginDatabaseAdapter(Context _context) {
        context = _context;
        dbHelper = new LoginHelp(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public  LoginDatabaseAdapter open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        db.close();
    }


    public  SQLiteDatabase getDatabaseInstance() {
        return db;
    }


    public void insertUser(String name, String email, String userName,String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("EMAIL", email);
        contentValues.put("USERNAME", userName);
        contentValues.put("PASSWORD",password);
        db.insert("LOGIN", null, contentValues);
        Toast.makeText(context, "User Info Saved", Toast.LENGTH_LONG).show();
    }


    public String getPassword(String username) {
        Cursor cursor = db.query("LOGIN", null, " USERNAME=?", new String[]{username}, null, null, null);
        if(cursor.getCount()<1)
            return "NOT EXISTS";
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("PASSWORD"));
        return password;
    }


    public void  updateEntry(String name, String email, String username, String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("EMAIL", email);
        contentValues.put("PASSWORD",password);
        String where="USERNAME = ?";
        db.update("LOGIN", contentValues, where, new String[]{username});
    }

    public User getUser(String username) {
        User user = new User();
        user.setUsername(username);
        Cursor cursor=db.query("LOGIN", null, " USERNAME=?", new String[]{username}, null, null, null);
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("PASSWORD"));
        user.setPassword(password);
        String name= cursor.getString(cursor.getColumnIndex("NAME"));
        user.setName(name);
        String email= cursor.getString(cursor.getColumnIndex("EMAIL"));
        user.setEmail(email);
        return user;
    }
}

package com.example.faza1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Profile extends AppCompatActivity {
    LoginDatabaseAdapter loginDataBaseAdapter;
    TextView tvName,tvEmail,tvUsername;
    Button update, grades, button, locatii;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        loginDataBaseAdapter = new LoginDatabaseAdapter(this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();

        tvName = (TextView) findViewById(R.id.textView4);
        tvEmail = (TextView) findViewById(R.id.textView5);
        tvUsername = (TextView) findViewById(R.id.textView6);


        String value;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("userName");
            tvUsername.setText(value);
            User user = loginDataBaseAdapter.getUser(value);
            tvName.setText(user.getName());
            tvEmail.setText(user.getEmail());
            extras = null;
        }

        /*update.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intentUpdateProfile = new Intent(getApplicationContext(), UpdateProfileActivity.class);
                intentUpdateProfile.putExtra("user", tvUsername.getText());
                startActivity(intentUpdateProfile);
            }
        });

        grades.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intentGrade = new Intent(getApplicationContext(), Grade.class);
                startActivity(intentGrade);
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.updateProfile){
            Intent intentUpdateProfile = new Intent(getApplicationContext(), UpdateProfileActivity.class);
            intentUpdateProfile.putExtra("user", tvUsername.getText());
            startActivity(intentUpdateProfile);
        }
        else if(item.getItemId() == R.id.viewGrades){
            startActivity(new Intent(Profile.this, Grade.class));
        }
        else if(item.getItemId() == R.id.news){
            startActivity(new Intent(Profile.this, DownloadTaskActivity.class));
        }
        else if(item.getItemId() == R.id.locations){
            startActivity(new Intent(Profile.this, PlacesActivity.class));
        }
        else if(item.getItemId() == R.id.homework){
            startActivity(new Intent(Profile.this, HomeworkActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}

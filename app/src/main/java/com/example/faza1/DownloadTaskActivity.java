package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DownloadTaskActivity extends AppCompatActivity {

    ArrayList<String> avizier = new ArrayList<String>();
    ArrayList<String> dataAnunt = new ArrayList<>();
    ArrayAdapter arrayAdapter, arrayAdapter2;
    ListView lvAvizier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_task);

        lvAvizier= (ListView)findViewById(R.id.lvAvizier);
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, avizier);
        lvAvizier.setAdapter(arrayAdapter);

        DownloadTask task = new DownloadTask();
        task.execute("https://api.myjson.com/bins/kx0qu");
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try{
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection)url.openConnection();

                InputStream is = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(is);
                int data = reader.read();
                while(data != -1){
                    char c = (char)data;
                    result += c;
                    data = reader.read();
                }
                return result;
            }catch(MalformedURLException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                String infoAvizier = jsonObject.getString("avizier");

                JSONArray jsonArray = new JSONArray(infoAvizier);
                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject jsonPart = jsonArray.getJSONObject(i);
                    String data = jsonPart.getString("date");
                    String info = jsonPart.getString("info");
                    avizier.add(info);
                    dataAnunt.add(info);
                    arrayAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

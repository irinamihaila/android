package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class MarkListActivity extends AppCompatActivity {

    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_list);

        tv = (TextView)findViewById(R.id.textView21);
        List<Mark> marksList = MainActivity.database.studentDao().getMarks();
        String info = "";
        for(Mark m : marksList){
            int idNota = m.getIdNota();
            String materie = m.getSubject();
            int valoare = m.getValue();
            int idStudent = m.getId();
            info = info+ "\n\n" + "Id nota: " + idNota + ", Materie: " + materie + ", Nota: " + valoare + ", idStudent: " + idStudent;
        }
        tv.setText(info);

    }
}

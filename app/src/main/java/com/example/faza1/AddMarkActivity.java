package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class AddMarkActivity extends AppCompatActivity {

    EditText etIdNota, etMaterie, etIdStudent;
    TextView tvNota;
    SeekBar nota;
    Button btnAdaugaNota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mark);

        etIdNota = (EditText)findViewById(R.id.editText6);
        etMaterie = (EditText)findViewById(R.id.editText7);
        etIdStudent = (EditText)findViewById(R.id.editText9);
        nota = (SeekBar)findViewById(R.id.seekbarNota);
        btnAdaugaNota = (Button) findViewById(R.id.btnAddMark);
        tvNota=(TextView)findViewById(R.id.textViewNota);


        nota.setMax(10);

        nota.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress_value;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value = progress;
                tvNota.setText(String.valueOf(progress_value));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnAdaugaNota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<Mark> marks = new ArrayList<>();
//                marks = MainActivity.database.studentDao().getMarks();
//                for(int i=0; i<marks.size(); i++){
//                    if(marks.get(i).getId() == Integer.parseInt(etIdNota.getText().toString())){
//                        Toast.makeText(getApplicationContext(), "The grade id already exists. Please check" +
//                                "that you introduced an unique id!", Toast.LENGTH_LONG).show();
//                    }
//                }
                if(etIdNota.getText().toString().trim().length() == 0 || etMaterie.getText().toString().trim().length() == 0 || tvNota.getText().equals("") || etIdStudent.getText().toString().trim().length() == 0){
                    Toast.makeText(getApplicationContext(), "Please complete all fields!", Toast.LENGTH_LONG).show();
                }
                else {
                    int val;
                    int markId = Integer.parseInt(etIdNota.getText().toString());
                    String materie = etMaterie.getText().toString();
                    if(tvNota.getText().toString().equals(""))
                    {
                        val=-2;
                    }
                    else {
                        val = Integer.valueOf(tvNota.getText().toString());
                    }

                    int idStudent = Integer.parseInt(etIdStudent.getText().toString());

                    Mark mark = new Mark();
                    mark.setIdNota(markId);
                    mark.setSubject(materie);
                    mark.setValue(val);
                    mark.setId(idStudent);

                    MainActivity.database.studentDao().addMark(mark);
                    Toast.makeText(getApplicationContext(), "Mark was added!", Toast.LENGTH_LONG).show();
                    etIdStudent.setText("");
                    tvNota.setText("");
                    etMaterie.setText("");
                    etIdNota.setText("");
                    nota.setProgress(0);
                }
            }
        });
    }
}

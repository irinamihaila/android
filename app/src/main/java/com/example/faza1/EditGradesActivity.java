/*package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditGradesActivity extends AppCompatActivity {

    EditText editMaterie, editNota;
    TextView tvUserEdit;
    Student s;

    public void save(View v){
        s = MainActivity.database.studentDao().getStudent(tvUserEdit.getText().toString());
        Student m = new Student();
        m.setForename(editMaterie.getText().toString());
        m.setIdStudent(Integer.parseInt(String.valueOf(editNota.getText())));
        MainActivity.database.studentDao().updateStudent(editMaterie.getText().toString(), Integer.parseInt(String.valueOf(editNota.getText())), tvUserEdit.getText().toString());
        Toast.makeText(EditGradesActivity.this, "Student Updated", Toast.LENGTH_SHORT).show();
        Intent intentProfile=new Intent(getApplicationContext(), ShowGradesActivity.class);
        intentProfile.putExtra("userName", tvUserEdit.getText());
        startActivity(intentProfile);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_grades);

        editMaterie = (EditText)findViewById(R.id.etEditMaterie);
        editNota = (EditText)findViewById(R.id.etEditNota);
        tvUserEdit = (TextView)findViewById(R.id.tvUserEdit);

        Intent intent = getIntent();
        int gradeId = intent.getIntExtra("gradeId", -1);

        if(gradeId != -1){
            tvUserEdit.setText(ShowGradesActivity.students.get(gradeId).getName());
            editMaterie.setText(ShowGradesActivity.students.get(gradeId).getForename());
            editNota.setText(ShowGradesActivity.students.get(gradeId).getIdStudent());
        }
    }
}
*/
package com.example.faza1;

import android.view.LayoutInflater;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    public Context context;
    public List<StudentsWithMarks> note;

    public CustomAdapter(Context context, List<StudentsWithMarks> note){
        this.context = context;
        this.note = note;
    }

    @Override
    public int getCount() {
        return note.size();
    }

    @Override
    public Object getItem(int position) {
        return note.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.grade_list, parent, false);

            viewHolder.tvName = convertView.findViewById(R.id.tvName);
            viewHolder.tvNota = convertView.findViewById(R.id.tvSharedPreferences);
            viewHolder.tvMaterie = convertView.findViewById(R.id.tvSubject);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder)convertView.getTag();
        }


        StudentsWithMarks sm = note.get(position);
        viewHolder.tvName.setText(sm.student.getName() + " " + sm.student.getForename());
        List<Mark> listaNote = sm.marks;
        if(listaNote.size() == 0){
            viewHolder.tvName.setText(sm.student.getName() + " " + sm.student.getForename());
            viewHolder.tvNota.setText("-");
            viewHolder.tvMaterie.setText("-");
        }
        else{
            for(int i=0; i<listaNote.size(); i++){
                viewHolder.tvNota.setText(viewHolder.tvNota.getText().toString() + listaNote.get(i).getValue() + ", ");
                viewHolder.tvMaterie.setText(viewHolder.tvMaterie.getText() + listaNote.get(i).getSubject() + ", ");
            }
        }


        return convertView;
    }

    static class ViewHolder{
        TextView tvMaterie, tvName, tvNota;
    }
}
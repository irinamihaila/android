package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddStudentActivity extends AppCompatActivity {

    EditText etId, etName, etForename;
    Button btnSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        etId = (EditText) findViewById(R.id.editText3);
        etName = (EditText) findViewById(R.id.editText4);
        etForename = (EditText) findViewById(R.id.editText5);
        btnSave = (Button) findViewById(R.id.btnAddDatabase);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etName.getText().toString().trim().length() == 0 || etForename.getText().toString().trim().length() ==0 || etId.getText().toString().trim().length() == 0){
                    Toast.makeText(getApplicationContext(), "Please complete all fields!", Toast.LENGTH_LONG).show();
                }
                else {
                    int studentId = Integer.parseInt(etId.getText().toString());
                    String studentName = etName.getText().toString();
                    String studentForename = etForename.getText().toString();
                    Student student = new Student();
                    student.setIdStudent(studentId);
                    student.setForename(studentForename);
                    student.setName(studentName);

                    MainActivity.database.studentDao().addStudent(student);
                    Toast.makeText(getApplicationContext(), "Student was added!", Toast.LENGTH_LONG).show();
                    etName.setText("");
                    etForename.setText("");
                    etId.setText("");
                }
            }
        });
    }
}

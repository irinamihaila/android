package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class StudentListActivity extends AppCompatActivity {

    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        tv = (TextView)findViewById(R.id.tvListaStudenti);
        List<Student> students = MainActivity.database.studentDao().getStudents();
        String info = "";
        for(Student s : students){
            int id = s.getIdStudent();
            String name = s.getName();
            String forename = s.getForename();
            info = info+ "\n\n" + "Id: " + id + ", Name: " + name + ", Forename: " + forename;
        }
        tv.setText(info);

    }
}

package com.example.faza1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class PostHomeworkActivity extends AppCompatActivity {

    ImageButton ib;
    EditText et1, et2;
    Button btn;
    Uri imageUri;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private static final int GALLERY_REQUEST = 1;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_homework);

        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Homework");

        ib = (ImageButton)findViewById(R.id.ibHomework);
        et1 = (EditText)findViewById(R.id.etTitle);
        et2 = (EditText)findViewById(R.id.etDescription);
        btn = (Button)findViewById(R.id.btnPost);

        progressDialog = new ProgressDialog(this);

        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPosting();
            }
        });
    }

    public void startPosting(){
        progressDialog.setMessage("Posting homework...");
        progressDialog.show();

        final String title = et1.getText().toString().trim();
        final String description = et2.getText().toString().trim();

        if(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(description) && imageUri != null) {
            final StorageReference filepath = storageReference.child("Homework_Image").child(imageUri.getLastPathSegment());

            filepath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //String downloadURL = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    /*Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());
                    Uri downloadUrl = urlTask.getResult();*/

                    Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!urlTask.isSuccessful());
                    Uri downloadUrl = urlTask.getResult();
                    Log.i("info", downloadUrl.toString());

                    progressDialog.dismiss();
                    DatabaseReference newPost = databaseReference.push();
                    newPost.child("Title").setValue(title);
                    newPost.child("Description").setValue(description);
                    newPost.child("Image").setValue(downloadUrl.toString());
                }
            });
                startActivity(new Intent(PostHomeworkActivity.this, HomeworkActivity.class));
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            ib.setImageURI(imageUri);
        }
    }
}

package com.example.faza1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button login, register, admin;
    LoginDatabaseAdapter loginDatabaseAdapter;
    public static Database database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = Room.databaseBuilder(getApplicationContext(), Database.class, "studmarkDB").allowMainThreadQueries().build();

        loginDatabaseAdapter=new LoginDatabaseAdapter(this);
        loginDatabaseAdapter=loginDatabaseAdapter.open();

        login = (Button)findViewById(R.id.btnLogin);
        register = (Button)findViewById(R.id.btnRegister);
        admin = (Button)findViewById(R.id.btnAdmin);


        admin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent adminLogin = new Intent(getApplicationContext(),AdminLoginActivity.class);
                startActivity(adminLogin);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent register = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(register);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.location_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_location){
            startActivity(new Intent(MainActivity.this, PositionActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void signIn(View V) {

        final Dialog dialog = new Dialog(MainActivity.this);

        dialog.setContentView(R.layout.activity_login);
        dialog.setTitle("Login");
        dialog.setCanceledOnTouchOutside(true);

        final EditText editTextUserName=(EditText)dialog.findViewById(R.id.etName);
        final  EditText editTextPassword=(EditText)dialog.findViewById(R.id.etPass);

        Button btnSignIn=(Button)dialog.findViewById(R.id.btnLogin);

        btnSignIn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                String username=editTextUserName.getText().toString();
                String password=editTextPassword.getText().toString();
                String storedPass = loginDatabaseAdapter.getPassword(username);

                if(password.equals(storedPass)) {
                    Toast.makeText(MainActivity.this, "Welcome, " + username.toString() + "!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(),Profile.class);
                    intent.putExtra("userName",username); //userName
                    startActivity(intent);
                }
                else if(storedPass.equals("NOT EXISTS")) {
                    Toast.makeText(MainActivity.this, "Invalid username", Toast.LENGTH_SHORT).show();
                    editTextPassword.setText("");
                    editTextUserName.setText("");

                }
                else {
                    Toast.makeText(MainActivity.this, "Wrong password", Toast.LENGTH_SHORT).show();
                    editTextPassword.setText("");
                    editTextUserName.setText("");
                }
            }
        });
        dialog.show();
    }
}

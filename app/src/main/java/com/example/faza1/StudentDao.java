package com.example.faza1;

import androidx.annotation.WorkerThread;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Dao
public interface StudentDao {
    @Insert
    public void addStudent(Student student);

    @Transaction
    @Query("SELECT * FROM students")
    public List<StudentsWithMarks> getStudentsWithMarks();

    @Query("Delete FROM students where name LIKE  :nume")
    void deleteStudentMarks(String nume);

    @Query("Delete FROM marks where marks.id = :id")
    void deleteMarks(int id);

    @Query("SELECT * FROM students")
    public List<Student> getStudents();

    @Insert
    public void addMark(Mark mark);

    @Query("SELECT * FROM marks")
    public List<Mark> getMarks();

    @Query("SELECT * FROM marks WHERE marks.value >= 5")
    public List<Mark> getGoodMarks();

    @Query("SELECT subject, AVG(value) as average FROM marks GROUP BY subject")
    public List<SubjectAverage> getAverageMark();

    @Query("SELECT * FROM students WHERE students.name LIKE :nume")
    public Student getStudent(String nume);

    @Query("UPDATE students SET forename= :forename, idStudent= :idStudent WHERE name = :name")
    public void updateStudent(String forename, int idStudent, String name);

}

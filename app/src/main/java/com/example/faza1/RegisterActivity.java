package com.example.faza1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    EditText editTextUserName, editTextPassword, editTextConfirmPassword, editTextName, editTextEmail;
    Button btnCreateAccount;

    LoginDatabaseAdapter loginDataBaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        loginDataBaseAdapter = new LoginDatabaseAdapter(this);
        loginDataBaseAdapter = loginDataBaseAdapter.open();

        editTextUserName = (EditText) findViewById(R.id.etRUser);
        editTextPassword = (EditText) findViewById(R.id.etRPass);
        editTextConfirmPassword = (EditText) findViewById(R.id.etRConfirmPass);
        editTextName = (EditText) findViewById(R.id.etRName);
        editTextEmail = (EditText) findViewById(R.id.etREmail);
        btnCreateAccount = (Button) findViewById(R.id.btnR);


        btnCreateAccount.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                String name = editTextName.getText().toString();
                String email = editTextEmail.getText().toString();
                String userName = editTextUserName.getText().toString();
                String password = editTextPassword.getText().toString();
                String confirmPassword = editTextConfirmPassword.getText().toString();

                if (userName.equals("") || password.equals("") || confirmPassword.equals("") || name.equals("") || email.equals("")) {
                    Toast.makeText(getApplicationContext(), "Field Vaccant", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!password.equals(confirmPassword)) {
                    Toast.makeText(getApplicationContext(), "Password Does Not Matches", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    loginDataBaseAdapter.insertUser(name, email, userName, password);
                    Toast.makeText(getApplicationContext(), "Account Successfully Created ", Toast.LENGTH_LONG).show();
                    Intent intentSignUP = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intentSignUP);
                }


            }
        });
    }
}

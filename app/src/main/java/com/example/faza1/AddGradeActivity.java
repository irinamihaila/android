package com.example.faza1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class AddGradeActivity extends AppCompatActivity {

    EditText numeS, materie;
    Button adaugaNota, adaugaStudent, listaStudenti, listaNote, listaSN;
    SeekBar seekBar;
    TextView textView, tv;
    //GradeDatabaseAdapter gradeDatabaseAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.show_grade_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId() == R.id.home){
            Intent intentHome =new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intentHome);
            return true;
        }
        else
            if(item.getItemId() == R.id.showGrade){
                Intent intentGrade = new Intent(getApplicationContext(), ShowGradesActivity.class);
                startActivity(intentGrade);
                return true;
            }
            else
                if(item.getItemId() == R.id.raport){
                    Intent intentReport = new Intent(getApplicationContext(), MarkReportActivity.class);
                    startActivity(intentReport);
                    return true;
                }
                else
                    if(item.getItemId() == R.id.badMarks){
                        Intent intentReport = new Intent(getApplicationContext(), AveragePerSubjectActivity.class);
                        startActivity(intentReport);
                        return true;
                    }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_grade);

        listaStudenti = (Button)findViewById(R.id.button3);
        listaNote = (Button)findViewById(R.id.button4);
        adaugaNota = (Button)findViewById(R.id.btnAdaugaNota);
        textView = (TextView)findViewById(R.id.tvSharedPreferences);
        tv = (TextView)findViewById(R.id.tvSharedPref);
        adaugaStudent = (Button)findViewById(R.id.button2);

        SharedPreferences settings = getSharedPreferences("Save", Context.MODE_PRIVATE);
        String value = settings.getString("Value", "Data not found");

        tv.setText(value);

        adaugaStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAddStudent=new Intent(getApplicationContext(), AddStudentActivity.class);
                startActivity(intentAddStudent);
            }
        });

        listaStudenti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentListaStudenti = new Intent(getApplicationContext(), StudentListActivity.class);
                startActivity(intentListaStudenti);
            }
        });

        listaNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentListaStudenti = new Intent(getApplicationContext(), MarkListActivity.class);
                startActivity(intentListaStudenti);
            }
        });


        adaugaNota.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intentAddGrade=new Intent(getApplicationContext(), AddMarkActivity.class);
                startActivity(intentAddGrade);
            }
        });

    }
}

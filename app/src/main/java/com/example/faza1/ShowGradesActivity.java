package com.example.faza1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ShowGradesActivity extends AppCompatActivity {
    ListView lv;
    static CustomAdapter c;
    static List<StudentsWithMarks> note = new ArrayList<>();
    static List<Student> students = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_grades);

        lv = (ListView)findViewById(R.id.lvGrades);
        note = MainActivity.database.studentDao().getStudentsWithMarks();
        students = MainActivity.database.studentDao().getStudents();
        c = new CustomAdapter(this, note);
        lv.setAdapter(c);

        /*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent gradeEditor = new Intent(getApplicationContext(), EditGradesActivity.class);
                gradeEditor.putExtra("gradeId", position);
                startActivity(gradeEditor);
            }
        });*/


        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(ShowGradesActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Are you sure?")
                        .setMessage("Do you really want to delete this student?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MainActivity.database.studentDao().deleteStudentMarks(note.get(position).student.getName());
                                MainActivity.database.studentDao().deleteMarks(note.get(position).student.getIdStudent());
                                note.remove(position);
                                c.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                return true;
            }
        });

    }
}

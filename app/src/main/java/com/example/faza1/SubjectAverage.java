package com.example.faza1;

public class SubjectAverage {
    private String subject;
    private double average;

    public String getSubject() {
        return subject;
    }

    public double getAverage() {
        return average;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    @Override
    public String toString() {
        return "Subject: '" + subject +
                ", marks average: " + average;
    }
}
